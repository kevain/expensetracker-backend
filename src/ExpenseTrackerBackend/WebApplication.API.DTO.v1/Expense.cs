﻿using System;
using System.ComponentModel.DataAnnotations;

namespace kevain.ExpenseTracker.WebApplication.API.DTO.v1
{
    public class ExpenseCreateDTO
    {
        [Required]
        [MinLength(1), MaxLength(150)]
        public string Title { get; set; } = default!;

        [MaxLength(4096)] public string? Description { get; set; }
        public double Sum { get; set; }
        public DateTime TimeStamp { get; set; }

        public Guid ExpenseCategoryId { get; set; }
    }

    public class ExpenseUpdateDTO : ExpenseCreateDTO
    {
        [Required] public Guid Id { get; set; }
    }

    public class ExpenseDTO : ExpenseUpdateDTO
    {
        [Required] public Guid AppUserId { get; set; }

        public static ExpenseDTO FromCreateDTO(ExpenseCreateDTO createDTO) =>
            new ExpenseDTO
            {
                Title = createDTO.Title,
                Description = createDTO.Description,
                Sum = createDTO.Sum,
                TimeStamp = createDTO.TimeStamp,
                ExpenseCategoryId = createDTO.ExpenseCategoryId
            };

        public static ExpenseDTO FromUpdateDTO(ExpenseUpdateDTO updateDTO) =>
            new ExpenseDTO
            {
                Title = updateDTO.Title,
                Description = updateDTO.Description,
                Sum = updateDTO.Sum,
                TimeStamp = updateDTO.TimeStamp,
                ExpenseCategoryId = updateDTO.ExpenseCategoryId,
                Id = updateDTO.Id
            };
    }
}