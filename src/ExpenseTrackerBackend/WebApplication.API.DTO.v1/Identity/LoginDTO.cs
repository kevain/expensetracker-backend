﻿using System.ComponentModel.DataAnnotations;

namespace kevain.ExpenseTracker.WebApplication.API.DTO.v1.Identity
{
    public class LoginDTO
    {
        [Required]
        [MinLength(5), MaxLength(15)]
        public string Username { get; set; } = default!;

        [Required] public string Password { get; set; } = default!;
    }
}