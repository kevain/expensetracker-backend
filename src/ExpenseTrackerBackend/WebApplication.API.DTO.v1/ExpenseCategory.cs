﻿using System;
using System.ComponentModel.DataAnnotations;

namespace kevain.ExpenseTracker.WebApplication.API.DTO.v1
{
    public class ExpenseCategoryCreateDTO
    {
        [Required]
        [MinLength(1), MaxLength(50)]
        public string Title { get; set; } = default!;
    }

    public class ExpenseCategoryUpdateDTO : ExpenseCategoryCreateDTO
    {
        [Required] public Guid Id { get; set; }
    }

    public class ExpenseCategoryDTO : ExpenseCategoryUpdateDTO
    {
        [Required] public Guid AppUserId { get; set; }

        public static ExpenseCategoryDTO FromCreateDTO(ExpenseCategoryCreateDTO createDTO) =>
            new ExpenseCategoryDTO {Title = createDTO.Title};

        public static ExpenseCategoryDTO FromUpdateDTO(ExpenseCategoryUpdateDTO updateDTO) =>
            new ExpenseCategoryDTO
            {
                Id = updateDTO.Id,
                Title = updateDTO.Title
            };
    }
}