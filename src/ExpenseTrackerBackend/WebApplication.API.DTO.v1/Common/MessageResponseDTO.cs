﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kevain.ExpenseTracker.WebApplication.API.DTO.v1.Common
{
    public class MessageResponseDTO
    {
        [Required] public string Error { get; set; } = default!;

        // ReSharper disable once CollectionNeverQueried.Global
        public ICollection<string>? Messages { get; set; }

        public MessageResponseDTO()
        {
        }

        public MessageResponseDTO(string errorMessage)
        {
            Error = errorMessage;
        }
    }
}