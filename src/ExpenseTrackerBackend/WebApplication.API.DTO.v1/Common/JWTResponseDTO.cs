﻿using System.ComponentModel.DataAnnotations;

namespace kevain.ExpenseTracker.WebApplication.API.DTO.v1.Common
{
    public class JWTResponseDTO
    {
        [Required] public string Token { get; set; } = default!;
        [Required] public string Message { get; set; } = default!;
    }
}