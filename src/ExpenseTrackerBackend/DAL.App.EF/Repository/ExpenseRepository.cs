﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.DAL.Base.EF.Repository;
using kevain.BasePackages.Mapper.Base;
using kevain.ExpenseTracker.Contracts.DAL.App.Repository;
using kevain.ExpenseTracker.DAL.App.DTO;
using Microsoft.EntityFrameworkCore;

namespace kevain.ExpenseTracker.DAL.App.EF.Repository
{
    public class ExpenseRepository : EFBaseRepository<Guid, AppDbContext, Domain.Expense, DAL.App.DTO.Expense>,
        IExpenseRepository
    {
        public ExpenseRepository(AppDbContext repositoryDbContext) : base(
            repositoryDbContext, new BaseMapper<Domain.Expense, DTO.Expense>())
        {
        }

        public bool ExpenseBelongsToUser(Guid expenseId, Guid userId)
        {
            return PrepareQuery().Any(expense => expense.Id.Equals(expenseId) && expense.AppUserId.Equals(userId));
        }

        public async Task<bool> ExpenseBelongsToUserAsync(Guid expenseId, Guid userId)
        {
            return await PrepareQuery()
                .AnyAsync(expense => expense.Id.Equals(expenseId) &&
                                     expense.AppUserId.Equals(userId));
        }

        public IEnumerable<Expense> GetExpensesByCategory(Guid categoryId, Guid userId)
        {
            var query = PrepareQuery().Where(e => e.ExpenseCategoryId.Equals(categoryId) &&
                                                  e.AppUserId.Equals(userId));

            var domainEntities = query.ToList();

            return domainEntities.Select(e => Mapper.Map(e));
        }

        public async Task<IEnumerable<Expense>> GetExpensesByCategoryAsync(Guid categoryId, Guid userId)
        {
            var query = PrepareQuery().Where(e => e.ExpenseCategoryId.Equals(categoryId) &&
                                                  e.AppUserId.Equals(userId));

            var domainEntities = await query.ToListAsync();

            return domainEntities.Select(e => Mapper.Map(e));
        }
    }
}