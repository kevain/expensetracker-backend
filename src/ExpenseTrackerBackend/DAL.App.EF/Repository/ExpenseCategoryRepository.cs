﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.DAL.Base.EF.Repository;
using kevain.BasePackages.Mapper.Base;
using kevain.ExpenseTracker.Contracts.DAL.App.Repository;
using kevain.ExpenseTracker.DAL.App.DTO;
using Microsoft.EntityFrameworkCore;

namespace kevain.ExpenseTracker.DAL.App.EF.Repository
{
    public class ExpenseCategoryRepository :
        EFBaseRepository<Guid, AppDbContext, Domain.ExpenseCategory, DTO.ExpenseCategory>,
        IExpenseCategoryRepository
    {
        public ExpenseCategoryRepository(AppDbContext repositoryDbContext) : base(repositoryDbContext,
            new BaseMapper<Domain.ExpenseCategory, DTO.ExpenseCategory>())
        {
        }

        public IEnumerable<ExpenseCategory> GetAllExpenseCategories(Guid userId)
        {
            var query = PrepareQuery();

            query = query.Where(expenseCategory => expenseCategory.AppUserId.Equals(userId));
            var domainEntities = query.ToList();

            return domainEntities.Select(expenseCategory => Mapper.Map(expenseCategory));
        }

        public async Task<IEnumerable<ExpenseCategory>> GetAllExpenseCategoriesAsync(Guid userId)
        {
            var query = PrepareQuery();

            query = query.Where(expenseCategory => expenseCategory.AppUserId.Equals(userId));
            var domainEntities = await query.ToListAsync();

            return domainEntities.Select(expenseCategory => Mapper.Map(expenseCategory));
        }

        public bool ExpenseCategoryBelongsToUser(Guid expenseCategoryId, Guid userId)
        {
            return PrepareQuery().Any(expenseCategory =>
                expenseCategory.Id.Equals(expenseCategoryId) && expenseCategory.AppUserId.Equals(userId));
        }

        public async Task<bool> ExpenseCategoryBelongsToUserAsync(Guid expenseCategoryId, Guid userId)
        {
            return await PrepareQuery().AnyAsync(expenseCategory =>
                expenseCategory.Id.Equals(expenseCategoryId) && expenseCategory.AppUserId.Equals(userId));
        }

        public bool ExpenseCategoryTitleExists(string categoryTitle, Guid userId)
        {
            return PrepareQuery().Any(expenseCategory => expenseCategory.AppUserId.Equals(userId) &&
                                                         expenseCategory.Title.ToLower()
                                                             .Equals(categoryTitle.ToLower()));
        }

        public async Task<bool> ExpenseCategoryTitleExistsAsync(string categoryTitle, Guid userId)
        {
            return await PrepareQuery().AnyAsync(expenseCategory =>
                expenseCategory.AppUserId.Equals(userId) &&
                expenseCategory.Title.ToLower().Equals(categoryTitle.ToLower()));
        }
    }
}