﻿using kevain.BasePackages.DAL.Base.EF.UnitOfWork;
using kevain.ExpenseTracker.Contracts.DAL.App.Repository;
using kevain.ExpenseTracker.Contracts.DAL.App.UnitOfWork;
using kevain.ExpenseTracker.DAL.App.EF.Repository;

namespace kevain.ExpenseTracker.DAL.App.EF
{
    public class AppUnitOfWork : EFBaseUnitOfWork<AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfWork(AppDbContext uoWDbContext) : base(uoWDbContext)
        {
        }

        public IExpenseRepository Expenses =>
            GetRepository<IExpenseRepository>(() => new ExpenseRepository(UoWDbContext));

        public IExpenseCategoryRepository ExpenseCategories =>
            GetRepository<IExpenseCategoryRepository>(() => new ExpenseCategoryRepository(UoWDbContext));
    }
}