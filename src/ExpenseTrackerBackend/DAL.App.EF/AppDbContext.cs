﻿using System;
using kevain.ExpenseTracker.Domain;
using kevain.ExpenseTracker.Domain.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace kevain.ExpenseTracker.DAL.App.EF
{
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public DbSet<Expense> Expenses { get; set; } = default!;
        public DbSet<ExpenseCategory> ExpenseCategories { get; set; } = default!;

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Avoid multiple cascade paths. 
            builder.Entity<ExpenseCategory>()
                .HasMany<Expense>(category => category.Expenses)
                .WithOne(expense => expense.ExpenseCategory!)
                .HasForeignKey(expense => expense.ExpenseCategoryId)
                .OnDelete(DeleteBehavior.Restrict);
            // DeleteBehavior.Restrict -> if the parent (ExpenseCategory) has any children (Expenses),
            // the children must be deleted before the parent can be deleted. Otherwise exception will be thrown.
        }
    }
}