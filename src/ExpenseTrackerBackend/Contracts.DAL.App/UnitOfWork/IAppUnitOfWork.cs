﻿using kevain.BasePackages.Contracts.DAL.Base.UnitOfWork;
using kevain.ExpenseTracker.Contracts.DAL.App.Repository;

namespace kevain.ExpenseTracker.Contracts.DAL.App.UnitOfWork
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        IExpenseRepository Expenses { get; }
        IExpenseCategoryRepository ExpenseCategories { get; }
    }
}