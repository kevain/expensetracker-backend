﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace kevain.ExpenseTracker.Contracts.DAL.App.Repository
{
    public interface IExpenseCustomRepository<TExpense>
    {
        bool ExpenseBelongsToUser(Guid expenseId, Guid userId);
        Task<bool> ExpenseBelongsToUserAsync(Guid expenseId, Guid userId);

        IEnumerable<TExpense> GetExpensesByCategory(Guid categoryId, Guid userId);
        Task<IEnumerable<TExpense>> GetExpensesByCategoryAsync(Guid categoryId, Guid userId);
    }
}