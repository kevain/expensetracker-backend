﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace kevain.ExpenseTracker.Contracts.DAL.App.Repository
{
    public interface IExpenseCategoryCustomRepository<TExpenseCategory>
    {
        IEnumerable<TExpenseCategory> GetAllExpenseCategories(Guid userId);
        Task<IEnumerable<TExpenseCategory>> GetAllExpenseCategoriesAsync(Guid userId);
        bool ExpenseCategoryBelongsToUser(Guid expenseCategoryId, Guid userId);
        Task<bool> ExpenseCategoryBelongsToUserAsync(Guid expenseCategoryId, Guid userId);
        bool ExpenseCategoryTitleExists(string categoryTitle, Guid userId);
        Task<bool> ExpenseCategoryTitleExistsAsync(string categoryTitle, Guid userId);
    }
}