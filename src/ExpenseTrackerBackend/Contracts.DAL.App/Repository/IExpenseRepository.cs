﻿using System;
using kevain.BasePackages.Contracts.DAL.Base.Repository;
using kevain.ExpenseTracker.DAL.App.DTO;

namespace kevain.ExpenseTracker.Contracts.DAL.App.Repository
{
    public interface IExpenseRepository : IBaseRepository<Guid, Expense>, IExpenseCustomRepository<Expense>
    {
    }
}