﻿using System;
using kevain.BasePackages.Contracts.Domain.Base;

namespace kevain.ExpenseTracker.BLL.App.DTO
{
    public class Expense : IDomainEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid ExpenseCategoryId { get; set; }
        public Guid AppUserId { get; set; }

        public string Title { get; set; } = default!;
        public string Description { get; set; } = default!;
        public double Sum { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}