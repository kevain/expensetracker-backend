﻿using System;
using kevain.BasePackages.Contracts.Domain.Base;

namespace kevain.ExpenseTracker.DAL.App.DTO
{
    public class ExpenseCategory : IDomainEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid AppUserId { get; set; }

        public string Title { get; set; } = default!;
    }
}