﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace kevain.ExpenseTracker.Domain.Identity
{
    public class AppUser : IdentityUser<Guid>
    {
        public ICollection<Expense>? Expenses { get; set; }
        public ICollection<ExpenseCategory>? ExpenseCategories { get; set; }
    }
}