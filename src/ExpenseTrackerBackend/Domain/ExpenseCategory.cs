﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using kevain.ExpenseTracker.Domain.Base;
using kevain.ExpenseTracker.Domain.Identity;

namespace kevain.ExpenseTracker.Domain
{
    public class ExpenseCategory : DomainEntity
    {
        [Required]
        [MinLength(1), MaxLength(50)]
        public string Title { get; set; } = default!;

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

        public ICollection<Expense>? Expenses { get; set; }
    }
}