﻿using System;
using System.ComponentModel.DataAnnotations;
using kevain.BasePackages.Contracts.Domain.Base;

namespace kevain.ExpenseTracker.Domain.Base
{
    public abstract class DomainEntity : IDomainEntity<Guid>
    {
        [Key] public Guid Id { get; set; }
    }
}