﻿using System;
using System.ComponentModel.DataAnnotations;
using kevain.ExpenseTracker.Domain.Base;
using kevain.ExpenseTracker.Domain.Identity;

namespace kevain.ExpenseTracker.Domain
{
    public class Expense : DomainEntity
    {
        [Required]
        [MinLength(1), MaxLength(150)]
        public string Title { get; set; } = default!;

        [MaxLength(4096)] public string? Description { get; set; }
        public double Sum { get; set; }
        public DateTime TimeStamp { get; set; }

        public Guid ExpenseCategoryId { get; set; }
        public ExpenseCategory? ExpenseCategory { get; set; }

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }
    }
}