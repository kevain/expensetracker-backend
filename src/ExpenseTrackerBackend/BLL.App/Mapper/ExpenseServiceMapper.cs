﻿using kevain.BasePackages.BLL.Base.Mappers;
using kevain.ExpenseTracker.BLL.App.DTO;
using kevain.ExpenseTracker.Contracts.BLL.App.Mapper;

namespace kevain.ExpenseTracker.BLL.App.Mapper
{
    public class ExpenseServiceMapper : BaseMapper<DAL.App.DTO.Expense, Expense>, IExpenseServiceMapper
    {
    }
}