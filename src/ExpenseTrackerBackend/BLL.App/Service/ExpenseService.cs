﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.BLL.Base.Services;
using kevain.ExpenseTracker.BLL.App.DTO;
using kevain.ExpenseTracker.BLL.App.Exceptions;
using kevain.ExpenseTracker.BLL.App.Mapper;
using kevain.ExpenseTracker.Contracts.BLL.App.Mapper;
using kevain.ExpenseTracker.Contracts.BLL.App.Service;
using kevain.ExpenseTracker.Contracts.DAL.App.Repository;
using kevain.ExpenseTracker.Contracts.DAL.App.UnitOfWork;

namespace kevain.ExpenseTracker.BLL.App.Service
{
    public class ExpenseService : BaseEntityService<Guid, IAppUnitOfWork, IExpenseRepository, IExpenseServiceMapper,
        DAL.App.DTO.Expense, Expense>, IExpenseService
    {
        public ExpenseService(IAppUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.Expenses,
            new ExpenseServiceMapper())
        {
        }

        public bool ExpenseBelongsToUser(Guid expenseId, Guid userId)
        {
            return Repository.ExpenseBelongsToUser(expenseId, userId);
        }

        public async Task<bool> ExpenseBelongsToUserAsync(Guid expenseId, Guid userId)
        {
            return await Repository.ExpenseBelongsToUserAsync(expenseId, userId);
        }

        public Expense GetExpense(Guid expenseId, Guid userId)
        {
            if (!ExpenseBelongsToUser(expenseId, userId))
            {
                throw new ExpenseNotBelongingToAppUserException();
            }

            return base.FirstOrDefault(expenseId);
        }

        public async Task<Expense> GetExpenseAsync(Guid expenseId, Guid userId)
        {
            if (!await ExpenseBelongsToUserAsync(expenseId, userId))
            {
                throw new ExpenseNotBelongingToAppUserException();
            }

            return await base.FirstOrDefaultAsync(expenseId);
        }

        public IEnumerable<Expense> GetExpensesByCategory(Guid categoryId, Guid userId)
        {
            if (!UnitOfWork.ExpenseCategories.ExpenseCategoryBelongsToUser(categoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            return Repository.GetExpensesByCategory(categoryId, userId)
                .OrderBy(e => e.TimeStamp)
                .Select(e => Mapper.Map(e));
        }

        public async Task<IEnumerable<Expense>> GetExpensesByCategoryAsync(Guid categoryId, Guid userId)
        {
            if (!await UnitOfWork.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(categoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            return (await Repository.GetExpensesByCategoryAsync(categoryId, userId))
                .OrderBy(e => e.TimeStamp)
                .Select(e => Mapper.Map(e));
        }

        public Expense AddExpense(Expense expense, Guid userId)
        {
            if (!UnitOfWork.ExpenseCategories.ExpenseCategoryBelongsToUser(expense.ExpenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            expense.Sum = Math.Round(expense.Sum, 2);
            expense.AppUserId = userId;

            return base.Add(expense);
        }

        public async Task<Expense> AddExpenseAsync(Expense expense, Guid userId)
        {
            if (!await UnitOfWork.ExpenseCategories
                .ExpenseCategoryBelongsToUserAsync(expense.ExpenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            expense.Sum = Math.Round(expense.Sum, 2);
            expense.AppUserId = userId;

            return await base.AddAsync(expense);
        }

        public Expense UpdateExpense(Expense expense, Guid userId)
        {
            if (!ExpenseBelongsToUser(expense.Id, userId))
            {
                throw new ExpenseNotBelongingToAppUserException();
            }

            if (!UnitOfWork.ExpenseCategories.ExpenseCategoryBelongsToUser(expense.ExpenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            expense.Sum = Math.Round(expense.Sum, 2);
            expense.AppUserId = userId;

            return base.Update(expense);
        }

        public async Task<Expense> UpdateExpenseAsync(Expense expense, Guid userId)
        {
            if (!await ExpenseBelongsToUserAsync(expense.Id, userId))
            {
                throw new ExpenseNotBelongingToAppUserException();
            }

            if (!await UnitOfWork.ExpenseCategories
                .ExpenseCategoryBelongsToUserAsync(expense.ExpenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            expense.Sum = Math.Round(expense.Sum, 2);
            expense.AppUserId = userId;

            return base.Update(expense);
        }

        public Expense RemoveExpense(Expense expense, Guid userId) => RemoveExpense(expense.Id, userId);

        public async Task<Expense> RemoveExpenseAsync(Expense expense, Guid userId) =>
            await RemoveExpenseAsync(expense.Id, userId);

        public Expense RemoveExpense(Guid expenseId, Guid userId)
        {
            if (!ExpenseBelongsToUser(expenseId, userId))
            {
                throw new ExpenseNotBelongingToAppUserException();
            }

            return base.Remove(expenseId);
        }

        public async Task<Expense> RemoveExpenseAsync(Guid expenseId, Guid userId)
        {
            if (!await ExpenseBelongsToUserAsync(expenseId, userId))
            {
                throw new ExpenseNotBelongingToAppUserException();
            }

            return base.Remove(expenseId);
        }
    }
}