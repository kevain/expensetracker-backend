﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.BLL.Base.Services;
using kevain.ExpenseTracker.BLL.App.DTO;
using kevain.ExpenseTracker.BLL.App.Exceptions;
using kevain.ExpenseTracker.BLL.App.Mapper;
using kevain.ExpenseTracker.Contracts.BLL.App.Mapper;
using kevain.ExpenseTracker.Contracts.BLL.App.Service;
using kevain.ExpenseTracker.Contracts.DAL.App.Repository;
using kevain.ExpenseTracker.Contracts.DAL.App.UnitOfWork;

namespace kevain.ExpenseTracker.BLL.App.Service
{
    public class ExpenseCategoryService : BaseEntityService<Guid, IAppUnitOfWork, IExpenseCategoryRepository,
        IExpenseCategoryServiceMapper,
        DAL.App.DTO.ExpenseCategory, ExpenseCategory>, IExpenseCategoryService
    {
        public ExpenseCategoryService(IAppUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.ExpenseCategories,
            new ExpenseCategoryServiceMapper())
        {
        }

        public bool ExpenseCategoryBelongsToUser(Guid expenseCategoryId, Guid userId) =>
            Repository.ExpenseCategoryBelongsToUser(expenseCategoryId, userId);

        public async Task<bool> ExpenseCategoryBelongsToUserAsync(Guid expenseCategoryId, Guid userId) =>
            await Repository.ExpenseCategoryBelongsToUserAsync(expenseCategoryId, userId);

        public ExpenseCategory GetExpenseCategoryById(Guid expenseCategoryId, Guid userId)
        {
            if (!ExpenseCategoryBelongsToUser(expenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            return base.FirstOrDefault(expenseCategoryId);
        }

        public async Task<ExpenseCategory> GetExpenseCategoryByIdAsync(Guid expenseCategoryId, Guid userId)
        {
            if (!await ExpenseCategoryBelongsToUserAsync(expenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            return await base.FirstOrDefaultAsync(expenseCategoryId);
        }

        public IEnumerable<ExpenseCategory> GetAllExpenseCategories(Guid userId) => Repository
            .GetAllExpenseCategories(userId).Select(expenseCategory => Mapper.Map(expenseCategory));

        public async Task<IEnumerable<ExpenseCategory>> GetAllExpenseCategoriesAsync(Guid userId) =>
            (await Repository.GetAllExpenseCategoriesAsync(userId))
            .Select(expenseCategory => Mapper.Map(expenseCategory))
            .OrderBy(expenseCategory => expenseCategory.Title);

        public ExpenseCategory AddExpenseCategory(ExpenseCategory expenseCategory, Guid userId)
        {
            if (Repository.ExpenseCategoryTitleExists(expenseCategory.Title, userId))
            {
                throw new ExpenseCategoryTitleUniqueConstraintViolationException();
            }

            expenseCategory.AppUserId = userId;

            return base.Add(expenseCategory);
        }

        public async Task<ExpenseCategory> AddExpenseCategoryAsync(ExpenseCategory expenseCategory, Guid userId)
        {
            if (await Repository.ExpenseCategoryTitleExistsAsync(expenseCategory.Title, userId))
            {
                throw new ExpenseCategoryTitleUniqueConstraintViolationException();
            }

            expenseCategory.AppUserId = userId;

            return await base.AddAsync(expenseCategory);
        }

        public ExpenseCategory UpdateExpenseCategory(ExpenseCategory expenseCategory, Guid userId)
        {
            if (!ExpenseCategoryBelongsToUser(expenseCategory.Id, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            if (Repository.ExpenseCategoryTitleExists(expenseCategory.Title, userId))
            {
                throw new ExpenseCategoryTitleUniqueConstraintViolationException();
            }

            expenseCategory.AppUserId = userId;

            return base.Update(expenseCategory);
        }

        public async Task<ExpenseCategory> UpdateExpenseCategoryAsync(ExpenseCategory expenseCategory, Guid userId)
        {
            if (!await ExpenseCategoryBelongsToUserAsync(expenseCategory.Id, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            if (await Repository.ExpenseCategoryTitleExistsAsync(expenseCategory.Title, userId))
            {
                throw new ExpenseCategoryTitleUniqueConstraintViolationException();
            }

            expenseCategory.AppUserId = userId;

            return base.Update(expenseCategory);
        }

        public ExpenseCategory RemoveExpenseCategory(ExpenseCategory expenseCategory, Guid userId) =>
            RemoveExpenseCategory(expenseCategory.Id, userId);

        public async Task<ExpenseCategory> RemoveExpenseCategoryAsync(ExpenseCategory expenseCategory, Guid userId) =>
            await RemoveExpenseCategoryAsync(expenseCategory.Id, userId);

        public ExpenseCategory RemoveExpenseCategory(Guid expenseCategoryId, Guid userId)
        {
            if (!ExpenseCategoryBelongsToUser(expenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            return base.Remove(expenseCategoryId);
        }

        public async Task<ExpenseCategory> RemoveExpenseCategoryAsync(Guid expenseCategoryId, Guid userId)
        {
            if (!await ExpenseCategoryBelongsToUserAsync(expenseCategoryId, userId))
            {
                throw new ExpenseCategoryNotBelongingToAppUserException();
            }

            return base.Remove(expenseCategoryId);
        }

        public IEnumerable<ExpenseCategory> AddDefaultCategories(Guid userId)
        {
            var categoryHomeExpenses = new ExpenseCategory
            {
                Title = "Home Expenses",
                AppUserId = userId
            };

            var categoryCarExpenses = new ExpenseCategory
            {
                Title = "Car Expenses",
                AppUserId = userId
            };

            var categoryFoodExpenses = new ExpenseCategory
            {
                Title = "Food Expenses",
                AppUserId = userId
            };

            base.Add(categoryHomeExpenses);
            base.Add(categoryCarExpenses);
            base.Add(categoryFoodExpenses);

            return new[] {categoryHomeExpenses, categoryFoodExpenses, categoryCarExpenses};
        }

        public async Task<IEnumerable<ExpenseCategory>> AddDefaultCategoriesAsync(Guid userId)
        {
            var categoryHomeExpenses = new ExpenseCategory
            {
                Title = "Home Expenses",
                AppUserId = userId
            };

            var categoryCarExpenses = new ExpenseCategory
            {
                Title = "Car Expenses",
                AppUserId = userId
            };

            var categoryFoodExpenses = new ExpenseCategory
            {
                Title = "Food Expenses",
                AppUserId = userId
            };

            await base.AddAsync(categoryHomeExpenses);
            await base.AddAsync(categoryCarExpenses);
            await base.AddAsync(categoryFoodExpenses);

            return new[] {categoryHomeExpenses, categoryFoodExpenses, categoryCarExpenses};
        }
    }
}