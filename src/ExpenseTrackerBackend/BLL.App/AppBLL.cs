﻿using kevain.BasePackages.BLL.Base;
using kevain.ExpenseTracker.BLL.App.Service;
using kevain.ExpenseTracker.Contracts.BLL.App;
using kevain.ExpenseTracker.Contracts.BLL.App.Service;
using kevain.ExpenseTracker.Contracts.DAL.App.UnitOfWork;

namespace kevain.ExpenseTracker.BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        public AppBLL(IAppUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IExpenseService Expenses => GetService<IExpenseService>(() => new ExpenseService(UnitOfWork));

        public IExpenseCategoryService ExpenseCategories =>
            GetService<IExpenseCategoryService>(() => new ExpenseCategoryService(UnitOfWork));
    }
}