﻿using System;

namespace kevain.ExpenseTracker.BLL.App.Exceptions
{
    public class ExpenseNotBelongingToAppUserException : Exception
    {
        public ExpenseNotBelongingToAppUserException() : base("Expense does not exist!")
        {
        }

        public ExpenseNotBelongingToAppUserException(string message) : base(message)
        {
        }

        public ExpenseNotBelongingToAppUserException(string message, Exception innerException) : base(message,
            innerException)
        {
        }
    }
}