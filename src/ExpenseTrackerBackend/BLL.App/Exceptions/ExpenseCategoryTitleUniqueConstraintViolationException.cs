﻿using System;

namespace kevain.ExpenseTracker.BLL.App.Exceptions
{
    public class ExpenseCategoryTitleUniqueConstraintViolationException : Exception
    {
        public ExpenseCategoryTitleUniqueConstraintViolationException() : base("The title of an ExpenseCategory must be unique!")
        {
        }

        public ExpenseCategoryTitleUniqueConstraintViolationException(string message) : base(message)
        {
        }

        public ExpenseCategoryTitleUniqueConstraintViolationException(string message, Exception innerException) : base(message,
            innerException)
        {
        }
    }
}