﻿using System;

namespace kevain.ExpenseTracker.BLL.App.Exceptions
{
    public class ExpenseCategoryNotBelongingToAppUserException : Exception
    {
        public ExpenseCategoryNotBelongingToAppUserException(): base("ExpenseCategory does not exist!")
        {
        }

        public ExpenseCategoryNotBelongingToAppUserException(string message) : base(message)
        {
        }

        public ExpenseCategoryNotBelongingToAppUserException(string message, Exception innerException) : base(message,
            innerException)
        {
        }
    }
}