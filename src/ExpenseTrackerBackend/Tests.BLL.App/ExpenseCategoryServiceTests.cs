using System;
using System.Linq;
using System.Threading.Tasks;
using kevain.ExpenseTracker.BLL.App;
using kevain.ExpenseTracker.BLL.App.DTO;
using kevain.ExpenseTracker.Contracts.BLL.App;
using kevain.ExpenseTracker.Contracts.DAL.App.UnitOfWork;
using kevain.ExpenseTracker.DAL.App.EF;
using kevain.ExpenseTracker.Domain.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Faker;
using kevain.ExpenseTracker.BLL.App.Exceptions;

namespace kevain.ExpenseTracker.Tests.BLL.App
{
    [TestClass]
    public class ExpenseCategoryServiceTests
    {
        private IAppBLL _bll = null!;
        private IAppUnitOfWork _uow = null!;
        private AppDbContext _dbContext = null!;
        private AppUser _appUser = null!;

        // Server running in local Docker instance
        private const string SqliteConnectionStr = "Data Source=ExpenseTracker.tests.db";

        private static readonly DbContextOptionsBuilder<AppDbContext> DbContextOptions =
            new DbContextOptionsBuilder<AppDbContext>().UseSqlite(SqliteConnectionStr);

        [TestInitialize]
        public void Setup()
        {
            CreateContext();
            _dbContext.Database.EnsureCreated();

            _uow = new AppUnitOfWork(_dbContext);
            _bll = new AppBLL(_uow);

            _appUser = CreateUser();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _dbContext.Database.EnsureDeleted();
            DropContext();
        }

        #region helpers

        private void CreateContext() => _dbContext = new AppDbContext(DbContextOptions.Options);

        private void DropContext() => _dbContext.Dispose();

        private void ReloadContextAndUoWBll()
        {
            DropContext();
            CreateContext();

            _uow = new AppUnitOfWork(_dbContext);
            _bll = new AppBLL(_uow);
        }

        private AppUser CreateUser()
        {
            var appUser = new AppUser {UserName = Internet.UserName()};
            _dbContext.Users.Add(appUser);
            _dbContext.SaveChanges();

            return appUser;
        }

        private static ExpenseCategory CreateExpenseCategory(Guid? appUserId = null)
        {
            return new ExpenseCategory
            {
                Title = Faker.Internet.UserName(),
                AppUserId = appUserId.GetValueOrDefault()
            };
        }

        #endregion

        #region Tests_ExpenseCategoryBelongsToUserAsync

        [TestMethod]
        public async Task ExpenseCategoryBelongsToUserAsync_ExpenseCategoryWhichBelongsToCurrentUser_ReturnsTrue()
        {
            var expenseCategory = CreateExpenseCategory(_appUser.Id);

            expenseCategory = await _bll.ExpenseCategories.AddExpenseCategoryAsync(expenseCategory, _appUser.Id);
            await _bll.SaveChangesAsync();

            Assert.IsTrue(
                await _bll.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(expenseCategory.Id, _appUser.Id));
        }

        [TestMethod]
        public async Task ExpenseCategoryBelongsToUserAsync_ExpenseCategoryWhichBelongsToAnotherUser_ReturnsFalse()
        {
            var categoryOwnerUser = _appUser;
            var anotherUserId = Guid.NewGuid();
            var expenseCategory = CreateExpenseCategory(categoryOwnerUser.Id);

            expenseCategory =
                await _bll.ExpenseCategories.AddExpenseCategoryAsync(expenseCategory, categoryOwnerUser.Id);
            await _bll.SaveChangesAsync();

            Assert.IsFalse(
                await _bll.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(expenseCategory.Id, anotherUserId));
        }

        [TestMethod]
        public async Task ExpenseCategoryBelongsToUserAsync_ExpenseCategoryWhichDoesNotExist_ReturnsFalse()
        {
            var nonExistentCategoryId = Guid.NewGuid();

            Assert.IsFalse(
                await _bll.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(nonExistentCategoryId, _appUser.Id));
        }

        #endregion

        #region Tests_GetExpenseCategoryByIdAsync

        [TestMethod]
        public async Task GetExpenseCategoryByIdAsync_ExpenseCategoryDoesNotExistForUser_ThrowsException()
        {
            var nonExistentCategoryId = Guid.NewGuid();

            await Assert.ThrowsExceptionAsync<ExpenseCategoryNotBelongingToAppUserException>(async () =>
                await _bll.ExpenseCategories.GetExpenseCategoryByIdAsync(nonExistentCategoryId, _appUser.Id));
        }

        [TestMethod]
        public async Task GetExpenseCategoryByIdAsync_ExpenseCategoryBelongsToUser_ReturnsCorrectExpenseCategory()
        {
            var category = CreateExpenseCategory(_appUser.Id);

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, _appUser.Id);
            await _bll.SaveChangesAsync();

            var sameCategoryFromBll =
                await _bll.ExpenseCategories.GetExpenseCategoryByIdAsync(category.Id, _appUser.Id);

            Assert.AreEqual(category.Id, sameCategoryFromBll.Id);
        }

        [TestMethod]
        public async Task GetExpenseCategoryByIdAsync_ExpenseCategoryBelongsToDifferentUser_ThrowsException()
        {
            var categoryOwner = _appUser;
            var category = CreateExpenseCategory(categoryOwner.Id);
            var myUserId = Guid.NewGuid();

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, categoryOwner.Id);
            await _bll.SaveChangesAsync();

            await Assert.ThrowsExceptionAsync<ExpenseCategoryNotBelongingToAppUserException>(async () =>
            {
                await _bll.ExpenseCategories.GetExpenseCategoryByIdAsync(category.Id, myUserId);
            });
        }

        #endregion

        #region Tests_GetAllExpenseCategoriesAsync

        [TestMethod]
        public async Task GetAllExpenseCategoriesAsync_UserDoesNotHaveAnyExpenseCategories_ReturnsEmptyCollection()
        {
            var userCategories = await _bll.ExpenseCategories.GetAllExpenseCategoriesAsync(_appUser.Id);

            Assert.AreEqual(0, userCategories.Count());
        }

        [TestMethod]
        public async Task GetAllExpenseCategoriesAsync_ManyUsersHaveCategories_ReturnsOnlySpecifiedUserCategories()
        {
            const int correctNumberOfCategories = 5;
            var myUser = _appUser;
            var anotherUser = CreateUser();

            for (var i = 0; i < correctNumberOfCategories; i++)
            {
                await _bll.ExpenseCategories.AddExpenseCategoryAsync(CreateExpenseCategory(myUser.Id), myUser.Id);
                await _bll.ExpenseCategories.AddExpenseCategoryAsync(CreateExpenseCategory(anotherUser.Id),
                    anotherUser.Id);
            }

            await _bll.SaveChangesAsync();

            var myUserCategories = await _bll.ExpenseCategories.GetAllExpenseCategoriesAsync(myUser.Id);

            Assert.AreEqual(correctNumberOfCategories,
                myUserCategories.Count(expenseCategory => expenseCategory.AppUserId.Equals(myUser.Id)));
        }

        #endregion

        #region Tests_AddExpenseCategoryAsync

        [TestMethod]
        public async Task AddExpenseCategoryAsync_InitializeNewCategory_ReturnsObjectWithNonNullIdValue()
        {
            var category = CreateExpenseCategory(_appUser.Id);

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, _appUser.Id);
            await _bll.SaveChangesAsync();

            Assert.AreNotEqual(Guid.Empty, category.Id);
        }

        [TestMethod]
        public async Task AddExpenseCategoryAsync_DuplicateCategoryName_ThrowsException()
        {
            var category = CreateExpenseCategory(_appUser.Id);

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, _appUser.Id);
            await _bll.SaveChangesAsync();

            category = new ExpenseCategory
            {
                Title = category.Title,
                AppUserId = _appUser.Id
            };

            await Assert.ThrowsExceptionAsync<ExpenseCategoryTitleUniqueConstraintViolationException>(async () =>
            {
                await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, _appUser.Id);
            });
        }

        #endregion

        #region Tests_UpdateExpenseCategoryAsync

        [TestMethod]
        public async Task UpdateExpenseCategoryAsync_CategoryNameAlreadyUsed_ThrowsException()
        {
            var firstCategory = CreateExpenseCategory(_appUser.Id);
            var secondCategory = CreateExpenseCategory(_appUser.Id);

            firstCategory = await _bll.ExpenseCategories.AddExpenseCategoryAsync(firstCategory, _appUser.Id);
            secondCategory = await _bll.ExpenseCategories.AddExpenseCategoryAsync(secondCategory, _appUser.Id);
            await _bll.SaveChangesAsync();

            secondCategory.Title = firstCategory.Title;

            await Assert.ThrowsExceptionAsync<ExpenseCategoryTitleUniqueConstraintViolationException>(async () =>
            {
                await _bll.ExpenseCategories.UpdateExpenseCategoryAsync(secondCategory, _appUser.Id);
            });
        }

        [TestMethod]
        public async Task UpdateExpenseCategoryAsync_CategoryDoesNotBelongToUser_ThrowsException()
        {
            var categoryOwner = _appUser;
            var category = CreateExpenseCategory(categoryOwner.Id);
            var myUserId = Guid.NewGuid();

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, categoryOwner.Id);
            await _bll.SaveChangesAsync();

            category.Title = Lorem.GetFirstWord();

            await Assert.ThrowsExceptionAsync<ExpenseCategoryNotBelongingToAppUserException>(async () =>
            {
                await _bll.ExpenseCategories.UpdateExpenseCategoryAsync(category, myUserId);
            });
        }

        [TestMethod]
        public async Task UpdateExpenseCategoryAsync_CategoryBelongsToUser_ReturnsUpdatedObject()
        {
            var categoryOwner = _appUser;
            var category = CreateExpenseCategory(categoryOwner.Id);

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, categoryOwner.Id);
            await _bll.SaveChangesAsync();

            ReloadContextAndUoWBll(); // Reload to clear entity tracker 

            category.Title = Lorem.GetFirstWord();
            var updatedCategory = await _bll.ExpenseCategories.UpdateExpenseCategoryAsync(category, categoryOwner.Id);
            await _bll.SaveChangesAsync();

            Assert.AreEqual(category.Title, updatedCategory.Title);
        }

        #endregion

        #region Tests_RemoveExpenseCategoryAsync

        [TestMethod]
        public async Task RemoveExpenseCategoryAsync_CategoryBelongsToUser_ObjectGetsRemoved()
        {
            var categoryOwner = _appUser;
            var category = CreateExpenseCategory(categoryOwner.Id);

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, categoryOwner.Id);
            await _bll.SaveChangesAsync();

            ReloadContextAndUoWBll(); // Reload to clear entity tracker 

            var removedCategory = await _bll.ExpenseCategories.RemoveExpenseCategoryAsync(category, categoryOwner.Id);
            await _bll.SaveChangesAsync();

            Assert.IsFalse(
                await _bll.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(removedCategory.Id, categoryOwner.Id));
        }

        [TestMethod]
        public async Task RemoveExpenseCategoryAsync_CategoryBelongsToDifferentUser_ThrowsException()
        {
            var categoryOwner = _appUser;
            var category = CreateExpenseCategory(categoryOwner.Id);
            var myUserId = Guid.NewGuid();

            category = await _bll.ExpenseCategories.AddExpenseCategoryAsync(category, categoryOwner.Id);
            await _bll.SaveChangesAsync();

            await Assert.ThrowsExceptionAsync<ExpenseCategoryNotBelongingToAppUserException>(async () =>
            {
                await _bll.ExpenseCategories.RemoveExpenseCategoryAsync(category, myUserId);
            });
        }

        [TestMethod]
        public async Task RemoveExpenseCategoryAsync_CategoryDoesNotExist_ThrowsException()
        {
            var nonExistentCategoryId = Guid.NewGuid();

            await Assert.ThrowsExceptionAsync<ExpenseCategoryNotBelongingToAppUserException>(async () =>
            {
                await _bll.ExpenseCategories.RemoveExpenseCategoryAsync(nonExistentCategoryId, _appUser.Id);
            });
        }

        #endregion
    }
}