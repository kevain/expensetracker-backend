﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace kevain.ExpenseTracker.WebApplication.Helpers
{
    public static class JWTGenerator
    {
        public static string Generate(IEnumerable<Claim> claims, string signingKey, string issuer, int expirationInDays)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingKey));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512);
            var expirationEnd = DateTime.Now.AddDays(expirationInDays);

            var jwt = new JwtSecurityToken(issuer, issuer, claims, null, expirationEnd, signingCredentials);

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}