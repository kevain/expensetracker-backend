﻿using System;
using Microsoft.EntityFrameworkCore;

namespace kevain.ExpenseTracker.WebApplication.Helpers
{
    public static class DbOperations
    {
        public static void Migrate(DbContext dbContext) => dbContext.Database.Migrate();
    }
}