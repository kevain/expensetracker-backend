﻿using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using kevain.ExpenseTracker.Contracts.BLL.App;
using kevain.ExpenseTracker.Domain.Identity;
using kevain.ExpenseTracker.WebApplication.API.DTO.v1.Common;
using kevain.ExpenseTracker.WebApplication.API.DTO.v1.Identity;
using kevain.ExpenseTracker.WebApplication.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace kevain.ExpenseTracker.WebApplication.APIControllers.v1
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class IdentityController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<IdentityController> _logger;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IAppBLL _bll;

        public IdentityController(IConfiguration configuration, SignInManager<AppUser> signInManager,
            UserManager<AppUser> userManager, ILogger<IdentityController> logger, IAppBLL bll)
        {
            _configuration = configuration;
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            _bll = bll;
        }

        /// <summary>
        /// Authenticate with application.
        /// </summary>
        /// <param name="loginDTO">Login data</param>
        /// <response code="404">An user with matching credentials could not be found</response>
        /// <response code="200">User successfully authenticated</response>
        /// <returns>Generated JWT</returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JWTResponseDTO))]
        public async Task<ActionResult> Login([FromBody] LoginDTO loginDTO)
        {
            var appUser = await _userManager.FindByNameAsync(loginDTO.Username);

            if (appUser == null)
            {
                return NotFound(new MessageResponseDTO("User not found!"));
            }

            var signInResult = await _signInManager.CheckPasswordSignInAsync(appUser, loginDTO.Password, false);

            if (!signInResult.Succeeded)
            {
                return NotFound(new MessageResponseDTO("User not found!"));
            }

            return Ok(new JWTResponseDTO
            {
                Token = await GenerateJWT(appUser),
                Message = "Logged in."
            });
        }

        /// <summary>
        /// Register a new account in the system
        /// </summary>
        /// <param name="registerDTO">Registration data</param>
        /// <response code="200">User successfully registered</response>
        /// <response code="400">User registration failed</response>
        /// <returns>Generated JWT</returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JWTResponseDTO))]
        public async Task<ActionResult<string>> Register([FromBody] RegisterDTO registerDTO)
        {
            var appUser = await _userManager.FindByNameAsync(registerDTO.Username);

            if (appUser != null)
            {
                return BadRequest(new MessageResponseDTO("Username taken!"));
            }

            appUser = new AppUser
            {
                UserName = registerDTO.Username
            };

            var createUserResult = await _userManager.CreateAsync(appUser, registerDTO.Password);

            if (!createUserResult.Succeeded)
            {
                var response = new MessageResponseDTO
                {
                    Error = "Account creation failed!",
                    Messages = createUserResult.Errors
                        .Select(e => e.Description)
                        .ToList()
                };

                return BadRequest(response);
            }

            appUser = await _userManager.FindByNameAsync(registerDTO.Username);

            if (appUser == null)
            {
                return BadRequest(new MessageResponseDTO("Unexpected error."));
            }

            await _bll.ExpenseCategories.AddDefaultCategoriesAsync(appUser.Id);
            await _bll.SaveChangesAsync();

            return Ok(new JWTResponseDTO
            {
                Token = await GenerateJWT(appUser),
                Message = "Account Created."
            });
        }

        private async Task<string> GenerateJWT(AppUser appUser)
        {
            var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
            var signingKey = _configuration.GetValue<string>("JWT:SigningKey");
            var issuer = _configuration.GetValue<string>("JWT:Issuer");
            var expirationInDays = _configuration.GetValue<int>("JWT:ExpirationInDays");

            return JWTGenerator.Generate(claimsPrincipal.Claims, signingKey, issuer, expirationInDays);
        }
    }
}