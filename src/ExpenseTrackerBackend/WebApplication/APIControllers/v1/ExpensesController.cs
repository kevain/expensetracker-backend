﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using ee.kevain.IdentityExtensions;
using kevain.BasePackages.Contracts.Mapper.Base;
using kevain.BasePackages.Mapper.Base;
using kevain.ExpenseTracker.BLL.App.DTO;
using kevain.ExpenseTracker.BLL.App.Exceptions;
using kevain.ExpenseTracker.Contracts.BLL.App;
using kevain.ExpenseTracker.WebApplication.API.DTO.v1;
using kevain.ExpenseTracker.WebApplication.API.DTO.v1.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace kevain.ExpenseTracker.WebApplication.APIControllers.v1
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ExpensesController : Controller
    {
        private readonly IAppBLL _bll;
        private readonly ILogger<ExpensesController> _logger;
        private readonly IBaseMapper<ExpenseDTO, Expense> _mapper;

        public ExpensesController(IAppBLL bll, ILogger<ExpensesController> logger)
        {
            _bll = bll;
            _logger = logger;
            _mapper = new BaseMapper<ExpenseDTO, Expense>();
        }

        // GET: api/Expenses
        /// <summary>
        /// Get all expenses in category
        /// </summary>
        /// <param name="categoryId">ExpenseCategory ID</param>
        /// <response code="400">No ExpenseCategory ID provided</response>
        /// <response code="404">ExpenseCategory does not exist</response>
        /// <response code="200">Expenses successfully retrieved</response>
        /// <returns></returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ExpenseDTO>))]
        public async Task<ActionResult<IEnumerable<ExpenseDTO>>> GetExpensesByCategory(Guid categoryId)
        {
            if (categoryId.Equals(Guid.Empty))
            {
                return BadRequest(new MessageResponseDTO("No ExpenseCategory ID was provided."));
            }

            IEnumerable<Expense> bllExpenses;

            try
            {
                bllExpenses = await _bll.Expenses.GetExpensesByCategoryAsync(categoryId, User.GetUserGuid());
            }
            catch (ExpenseCategoryNotBelongingToAppUserException e)
            {
                return NotFound(new MessageResponseDTO(e.Message));
            }

            return Ok(bllExpenses.Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get Expense by ID
        /// </summary>
        /// <param name="id">ID of expense being queried</param>
        /// <response code="404">Expense not found</response>
        /// <response code="200">Expense successfully retrieved</response>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ExpenseDTO))]
        public async Task<ActionResult> GetExpense(Guid id)
        {
            Expense bllExpense;

            try
            {
                bllExpense = await _bll.Expenses.GetExpenseAsync(id, User.GetUserGuid());
            }
            catch (ExpenseNotBelongingToAppUserException e)
            {
                return NotFound(new MessageResponseDTO(e.Message));
            }

            return Ok(_mapper.Map(bllExpense));
        }

        /// <summary>
        /// Add new expense
        /// </summary>
        /// <param name="createDTO">Expense data</param>
        /// <response code="400">ExpenseCategory does not exist</response>
        /// <response code="201">Expense created</response>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ExpenseDTO))]
        public async Task<ActionResult> PostExpense([FromBody] ExpenseCreateDTO createDTO)
        {
            var bllEntity = _mapper.Map(ExpenseDTO.FromCreateDTO(createDTO));
            bllEntity.AppUserId = User.GetUserGuid();

            try
            {
                bllEntity = await _bll.Expenses.AddExpenseAsync(bllEntity, User.GetUserGuid());
            }
            catch (ExpenseCategoryNotBelongingToAppUserException e)
            {
                return BadRequest(new MessageResponseDTO(e.Message));
            }

            await _bll.SaveChangesAsync();

            var dto = _mapper.Map(bllEntity);

            return CreatedAtAction(nameof(GetExpense), new {id = dto.Id}, dto);
        }

        /// <summary>
        /// Update Expense
        /// </summary>
        /// <param name="id">ID of Expense to be updated</param>
        /// <param name="updateDTO">update data</param>
        /// <response code="400">ID's don't match</response>
        /// <response code="404">Expense or ExpenseCategory does not exist</response>
        /// <response code="204">Expense updated</response>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> PutExpense(Guid id, [FromBody] ExpenseUpdateDTO updateDTO)
        {
            if (id != updateDTO.Id)
            {
                return BadRequest(new MessageResponseDTO("ID mismatch error"));
            }

            if (!await _bll.Expenses.ExpenseBelongsToUserAsync(id, User.GetUserGuid()))
            {
                return NotFound(new MessageResponseDTO("Could not find expense with matching ID"));
            }

            var bllEntity = _mapper.Map(ExpenseDTO.FromUpdateDTO(updateDTO));

            bllEntity.AppUserId = User.GetUserGuid();

            try
            {
                await _bll.Expenses.UpdateExpenseAsync(bllEntity, User.GetUserGuid());
            }
            catch (ExpenseCategoryNotBelongingToAppUserException e)
            {
                return NotFound(new MessageResponseDTO(e.Message));
            }

            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Remove Expense
        /// </summary>
        /// <param name="id">Expense's ID</param>
        /// <response code="404">Expense does not exist</response>
        /// <response code="200">Expense removed</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MessageResponseDTO))]
        public async Task<ActionResult<MessageResponseDTO>> DeleteExpense(Guid id)
        {
            if (!await _bll.Expenses.ExpenseBelongsToUserAsync(id, User.GetUserGuid()))
            {
                return NotFound(new MessageResponseDTO("Could not find expense with given ID"));
            }

            await _bll.Expenses.RemoveExpenseAsync(id, User.GetUserGuid());
            await _bll.SaveChangesAsync();

            return Ok(new MessageResponseDTO("Expense removed."));
        }
    }
}