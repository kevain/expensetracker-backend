using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using ee.kevain.IdentityExtensions;
using kevain.BasePackages.Contracts.Mapper.Base;
using kevain.BasePackages.Mapper.Base;
using kevain.ExpenseTracker.BLL.App.DTO;
using kevain.ExpenseTracker.BLL.App.Exceptions;
using kevain.ExpenseTracker.Contracts.BLL.App;
using kevain.ExpenseTracker.WebApplication.API.DTO.v1;
using kevain.ExpenseTracker.WebApplication.API.DTO.v1.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace kevain.ExpenseTracker.WebApplication.APIControllers.v1
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ExpenseCategoriesController : Controller
    {
        private readonly IAppBLL _bll;
        private readonly ILogger<ExpenseCategoriesController> _logger;
        private readonly IBaseMapper<ExpenseCategoryDTO, ExpenseCategory> _mapper;

        public ExpenseCategoriesController(IAppBLL bll, ILogger<ExpenseCategoriesController> logger)
        {
            _bll = bll;
            _logger = logger;
            _mapper = new BaseMapper<ExpenseCategoryDTO, ExpenseCategory>();
        }

        // GET: api/ExpenseCategories
        /// <summary>
        /// Get all ExpenseCategories
        /// </summary>
        /// <response code="200">Successfully retrieved ExpenseCategories</response>
        /// <returns>Collection of ExpenseCategories for logged in user</returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ExpenseCategoryDTO>))]
        public async Task<ActionResult<IEnumerable<ExpenseCategoryDTO>>> GetExpenseCategories()
        {
            return Ok((await _bll.ExpenseCategories.GetAllExpenseCategoriesAsync(User.GetUserGuid()))
                .Select(expenseCategory => _mapper.Map(expenseCategory))
            );
        }

        // GET: api/ExpenseCategories/5
        /// <summary>
        /// Get an ExpenseCategory by it's ID
        /// </summary>
        /// <param name="id">ID of ExpenseCategory being queried</param>
        /// <response code="200">ExpenseCategory successfully retrieved</response>
        /// <response code="404">Could not find an ExpenseCategory with provided ID</response>
        /// <returns>ExpenseCategory with matching ID</returns>
        [HttpGet("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ExpenseCategoryDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageResponseDTO))]
        public async Task<ActionResult> GetExpenseCategory(Guid id)
        {
            if (await _bll.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(id, User.GetUserGuid()))
            {
                return Ok(_mapper.Map(
                    await _bll.ExpenseCategories.GetExpenseCategoryByIdAsync(id, User.GetUserGuid())
                ));
            }

            return NotFound(new MessageResponseDTO("Could not find expenseCategory with matching ID."));
        }

        // PUT: api/ExpenseCategories/5
        /// <summary>
        /// Modify ExpenseCategory
        /// </summary>
        /// <param name="id">ID of expenseCategory being updated</param>
        /// <param name="expenseCategory">Update data</param>
        /// <response code="400">Error performing update</response>
        /// <response code="404">Could not find ExpenseCategory with provided ID</response>
        /// <response code="204">ExpenseCategory updated</response>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> PutExpenseCategory(Guid id,
            [FromBody] ExpenseCategoryUpdateDTO expenseCategory)
        {
            if (id != expenseCategory.Id)
            {
                return BadRequest(new MessageResponseDTO("ID mismatch error"));
            }

            if (!await _bll.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(id, User.GetUserGuid()))
            {
                return NotFound(new MessageResponseDTO("Could not find expenseCategory with matching ID."));
            }

            var bllEntity = _mapper.Map(ExpenseCategoryDTO.FromUpdateDTO(expenseCategory));

            bllEntity.AppUserId = User.GetUserGuid();

            try
            {
                await _bll.ExpenseCategories.UpdateExpenseCategoryAsync(bllEntity, User.GetUserGuid());
            }
            catch (ExpenseCategoryTitleUniqueConstraintViolationException e)
            {
                return BadRequest(new MessageResponseDTO(e.Message));
            }

            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/ExpenseCategories
        /// <summary>
        /// Add new ExpenseCategory
        /// </summary>
        /// <param name="expenseCategory">Data of new ExpenseCategory</param>
        /// <response code="400">ExpenseCategory name already taken</response>
        /// <response code="201">ExpenseCategory created</response>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ExpenseCategoryDTO))]
        public async Task<ActionResult> PostExpenseCategory(
            [FromBody] ExpenseCategoryCreateDTO expenseCategory)
        {
            var bllEntity = _mapper.Map(ExpenseCategoryDTO.FromCreateDTO(expenseCategory));

            bllEntity.AppUserId = User.GetUserGuid();

            try
            {
                bllEntity = await _bll.ExpenseCategories.AddExpenseCategoryAsync(bllEntity, User.GetUserGuid());
            }
            catch (ExpenseCategoryTitleUniqueConstraintViolationException e)
            {
                return BadRequest(new MessageResponseDTO(e.Message));
            }

            await _bll.SaveChangesAsync();

            var dto = _mapper.Map(bllEntity);

            return CreatedAtAction(nameof(GetExpenseCategory), new {id = dto.Id}, dto);
        }

        // DELETE: api/ExpenseCategories/5
        /// <summary>
        /// Remove ExpenseCategory
        /// </summary>
        /// <param name="id">ID of ExpenseCategory being deleted</param>
        /// <response code="404">Could not find expenseCategory with provided ID</response>
        /// <response code="200">ExpenseCategory successfully deleted</response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(MessageResponseDTO))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MessageResponseDTO))]
        public async Task<ActionResult<MessageResponseDTO>> DeleteExpenseCategory(Guid id)
        {
            if (!await _bll.ExpenseCategories.ExpenseCategoryBelongsToUserAsync(id, User.GetUserGuid()))
            {
                return NotFound(new MessageResponseDTO("Could not find expenseCategory with provided ID"));
            }

            _bll.ExpenseCategories.Remove(id);
            await _bll.SaveChangesAsync();

            return Ok(new MessageResponseDTO("ExpenseCategory removed."));
        }
    }
}