﻿using kevain.BasePackages.Contracts.BLL.Base;
using kevain.ExpenseTracker.Contracts.BLL.App.Service;

namespace kevain.ExpenseTracker.Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        IExpenseService Expenses { get; }
        IExpenseCategoryService ExpenseCategories { get; }
    }
}