﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.BLL.Base.Services;
using kevain.ExpenseTracker.BLL.App.DTO;

namespace kevain.ExpenseTracker.Contracts.BLL.App.Service
{
    public interface IExpenseCategoryService : IBaseEntityService<Guid, ExpenseCategory>
    {
        bool ExpenseCategoryBelongsToUser(Guid expenseCategoryId, Guid userId);
        Task<bool> ExpenseCategoryBelongsToUserAsync(Guid expenseCategoryId, Guid userId);

        ExpenseCategory GetExpenseCategoryById(Guid expenseCategoryId, Guid userId);
        Task<ExpenseCategory> GetExpenseCategoryByIdAsync(Guid expenseCategoryId, Guid userId);

        IEnumerable<ExpenseCategory> GetAllExpenseCategories(Guid userId);
        Task<IEnumerable<ExpenseCategory>> GetAllExpenseCategoriesAsync(Guid userId);

        ExpenseCategory AddExpenseCategory(ExpenseCategory expenseCategory, Guid userId);
        Task<ExpenseCategory> AddExpenseCategoryAsync(ExpenseCategory expenseCategory, Guid userId);

        ExpenseCategory UpdateExpenseCategory(ExpenseCategory expenseCategory, Guid userId);
        Task<ExpenseCategory> UpdateExpenseCategoryAsync(ExpenseCategory expenseCategory, Guid userId);

        ExpenseCategory RemoveExpenseCategory(ExpenseCategory expenseCategory, Guid userId);
        Task<ExpenseCategory> RemoveExpenseCategoryAsync(ExpenseCategory expenseCategory, Guid userId);
        ExpenseCategory RemoveExpenseCategory(Guid expenseCategoryId, Guid userId);
        Task<ExpenseCategory> RemoveExpenseCategoryAsync(Guid expenseCategoryId, Guid userId);

        IEnumerable<ExpenseCategory> AddDefaultCategories(Guid userId);
        Task<IEnumerable<ExpenseCategory>> AddDefaultCategoriesAsync(Guid userId);
    }
}