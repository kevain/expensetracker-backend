﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.BLL.Base.Services;
using kevain.ExpenseTracker.BLL.App.DTO;

namespace kevain.ExpenseTracker.Contracts.BLL.App.Service
{
    public interface IExpenseService : IBaseEntityService<Guid, Expense>
    {
        bool ExpenseBelongsToUser(Guid expenseId, Guid userId);
        Task<bool> ExpenseBelongsToUserAsync(Guid expenseId, Guid userId);

        Expense GetExpense(Guid expenseId, Guid userId);
        Task<Expense> GetExpenseAsync(Guid expenseId, Guid userId);

        IEnumerable<Expense> GetExpensesByCategory(Guid categoryId, Guid userId);
        Task<IEnumerable<Expense>> GetExpensesByCategoryAsync(Guid categoryId, Guid userId);

        Expense AddExpense(Expense expense, Guid userId);
        Task<Expense> AddExpenseAsync(Expense expense, Guid userId);

        Expense UpdateExpense(Expense expense, Guid userId);
        Task<Expense> UpdateExpenseAsync(Expense expense, Guid userId);

        Expense RemoveExpense(Expense expense, Guid userId);
        Task<Expense> RemoveExpenseAsync(Expense expense, Guid userId);
        Expense RemoveExpense(Guid expenseId, Guid userId);
        Task<Expense> RemoveExpenseAsync(Guid expenseId, Guid userId);
    }
}