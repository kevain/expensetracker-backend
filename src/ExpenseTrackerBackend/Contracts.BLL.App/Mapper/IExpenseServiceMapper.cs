﻿using kevain.BasePackages.Contracts.BLL.Base.Mappers;
using kevain.ExpenseTracker.BLL.App.DTO;

namespace kevain.ExpenseTracker.Contracts.BLL.App.Mapper
{
    public interface IExpenseServiceMapper : IBaseMapper<DAL.App.DTO.Expense, Expense>
    {
    }
}