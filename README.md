# ExpenseTracker

## Goal
Develop my own REST API to use for learning various JS frontend frameworks and other technologies. 

## Requirements
User must be able to 

    1. login/register ✔
    2. insert & modify their expenses ✔
    3. create and modify categories ✔
    4. group expenses into categories ✔
    5. view monthly/yearly summaries

## External links
WebApp has been built into a docker container. [Docker Hub](https://hub.docker.com/repository/docker/kevain/expensetracker-aspnet).

Docker container has been deployed to Azure cloud service*. [Link to WebApplication](https://kevain-expensetracker.azurewebsites.net/).

\* _Hosted on free tier, may take a minute or two to load up at times._